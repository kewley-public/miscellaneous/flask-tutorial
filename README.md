# Flask Tutorial
Source code following the tutorial for Flask [here](http://flask.pocoo.org/docs/1.0/tutorial)

## Setup/Running locally
Make sure you execute `pip` in the root dir

```bash
pip install -e .
```

From here you can configure a `Flask` run configurations using your IDE, or perform the following
pending on your environment:

### Linux/Mac
```bash
export FLASK_APP=flaskr
export FLASK_ENV=development
flask run
```

### Windows
```bash
set FLASK_APP=flaskr
set FLASK_ENV=development
flask run
```

### Windows powershell
```bash
$env:FLASK_APP = "flaskr"
$env:FLASK_ENV = "development"
flask run
```

### Output
You should see output similar to this:
```bash
* Serving Flask app "flaskr"
* Environment: development
* Debug mode: on
* Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
* Restarting with stat
* Debugger is active!
* Debugger PIN: 855-212-761
```

## Running the Tests
Simply run the following command
```bash
pytest
```

If you wish to see a list of each function being ran run the following

```bash
pytest -v
```

For coverage reports
```bash
coverage run -m pytest
```
You can view the coverage report in the terminal:
```bash
coverage report
```
Or via html
```bash
coverage html # stores in htmlcov/index.html
```

## Deployment
### Build
When you want to deploy your application elsewhere, you build a distribution file. 
The current standard for Python distribution is the wheel format, with the .whl extension. 
Make sure the wheel library is installed first:

```bash
pip install wheel
```

Running setup.py with Python gives you a command line tool to issue build-related commands. 
The bdist_wheel command will build a wheel distribution file.

```bash
python setup.py bdist_wheel
```

You can find the file in dist/flaskr-1.0.0-py3-none-any.whl. The file name is the 
name of the project, the version, and some tags about the file can install.

Copy this file to another machine, set up a new virtualenv, then install the file with pip.

```bash
pip install flaskr-1.0.0-py3-none-any.whl
```

Pip will install your project along with its dependencies.

Since this is a different machine, you need to run init-db again to create the 
database in the instance folder.

```bash
export FLASK_APP=flaskr
flask init-db
```

When Flask detects that it’s installed (not in editable mode), it uses a different directory for the instance folder. 
You can find it at venv/var/flaskr-instance instead.

### Configure the Secret Key
In the beginning of the tutorial that you gave a default value for SECRET_KEY. 
This should be changed to some random bytes in production. Otherwise, attackers could use the 
public 'dev' key to modify the session cookie, or anything else that uses the secret key.

You can use the following command to output a random secret key:

```bash
python -c 'import os; print(os.urandom(16))'

b'_5#y2L"F4Q8z\n\xec]/'
```

Create the `config.py`file in the instance folder, which the factory will read from 
if it exists. Copy the generated value into it.

```bash
# venv/var/flaskr-instance/config.py
SECRET_KEY = b'_5#y2L"F4Q8z\n\xec]/'
```

__NOTE__: You can also set any other necessary configuration here, although `SECRET_KEY` is the only
needed one for Flaskr

### Running with a Production server
When running publicly rather than in development, you should not use the built-in development server (`flask run`). 
The development server is provided by Werkzeug for convenience, but is not designed to be 
particularly efficient, stable, or secure.

Instead, use a production WSGI server. For example, to use Waitress, first install it in the virtual environment:

```bash
pip install waitress
```

You need to tell Waitress about your application, but it doesn't use `FLASK_APP` like `flask run` does. You need
to tell it to import and call the application factory to get an application object

```bash
waitress-serve --call 'flaskr:create_app'
  Serving on http://0.0.0.0:8080
```
